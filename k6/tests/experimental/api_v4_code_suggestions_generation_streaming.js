/*global __ENV : true  */
/*
@endpoint: `POST /code_suggestions/completions`
@description: [Generate code suggestions - generation with streaming](https://docs.gitlab.com/ee/api/code_suggestions.html#generate-code-completions)
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@gpt_data_version: 1
@stressed_components: Rails, Postgres
*/

import http from "k6/http";
import { check } from "k6";
import { group } from "k6";
import { Rate } from "k6/metrics";
import {
  logError,
  getRpsThresholds,
  getTtfbThreshold,
} from "../../lib/gpt_k6_modules.js";

export let thresholds = {
  'rps': { 'latest': 0.05 },
  'ttfb': { 'latest': 1000 },
};
export let rpsThresholds = getRpsThresholds(thresholds['rps'])
export let ttfbThreshold = getTtfbThreshold(thresholds['ttfb'])
export let successRate = new Rate("successful_requests");
export let options = {
  thresholds: {
    successful_requests: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    checks: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    http_req_waiting: [`p(90)<${ttfbThreshold}`],
    http_reqs: [`count>=${rpsThresholds["count"]}`],
  },
};
// If Service Account PAT is used for GPT, AI tests require real user PAT which can be provided via AI_ACCESS_TOKEN
export const access_token = __ENV.AI_ACCESS_TOKEN !== null && __ENV.AI_ACCESS_TOKEN !== undefined ? __ENV.AI_ACCESS_TOKEN : __ENV.ACCESS_TOKEN;

export function setup() {
  console.log("");
  console.log(`RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`);
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
  console.log(`Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD) * 100}%`);
}

export default function () {
  group("API - Code Suggestions - Generation with streaming", function () {
    let params = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    };
    let body = {
      intent: "generation",
      current_file: {
        file_name: "main.py",
        content_above_cursor: "# generate a http server",
        content_below_cursor: "",
      },
      stream: true,
    };

    let res = http.post(
      `${__ENV.ENVIRONMENT_URL}/api/v4/code_suggestions/completions`,
      JSON.stringify(body),
      params
    );

    const checkOutput = check(res, {
      "is status 200": (r) => r.status === 200,
      "response is an event-stream": (r) => r.headers["Content-Type"].includes("event-stream"),
      "verify response body has some content": (r) => r.body !== ""
    });
    checkOutput ? successRate.add(true) : (successRate.add(false), logError(res));
  });
}
