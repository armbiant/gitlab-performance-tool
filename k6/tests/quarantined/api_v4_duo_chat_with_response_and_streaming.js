/*global __ENV : true */
/*
@endpoint: `POST /api/graphql`
@description: Send a question to Duo Chat and await the start of the streamed response
@gpt_data_version: 1
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@stressed_components: Rails, Postgres
*/

import http from "k6/http";
import {fail, group} from "k6";
import {Rate} from "k6/metrics";
import {getRpsThresholds, getTtfbThreshold} from "../../lib/gpt_k6_modules.js";
import {WebSocket} from 'k6/experimental/websockets';
import {randomString} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

export let thresholds = {
  'rps': { 'latest': 0.2 },
  'ttfb': { 'latest': 10000 },
};

export let rpsThresholds = getRpsThresholds(thresholds['rps'])
export let ttfbThreshold = getTtfbThreshold(thresholds['ttfb'])
export let wsDurationThreshold = __ENV.WS_DURATION_THRESHOLD !== null && __ENV.WS_DURATION_THRESHOLD !== undefined ? __ENV.WS_DURATION_THRESHOLD : 10000;

export let successRate = new Rate("successful_requests");
export let options = {
  thresholds: {
    successful_requests: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    checks: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    http_req_waiting: [`p(90)<${ttfbThreshold}`],
    http_reqs: [`count>=${rpsThresholds["count"]}`],
    ws_session_duration: [`p(90)<${wsDurationThreshold}`],
  },
};

// If Service Account PAT is used for GPT, AI tests require real user PAT which can be provided via AI_ACCESS_TOKEN
export const access_token = __ENV.AI_ACCESS_TOKEN !== null && __ENV.AI_ACCESS_TOKEN !== undefined ? __ENV.AI_ACCESS_TOKEN : __ENV.ACCESS_TOKEN;

export function buildWebSocketURL(){
  let webSocketProtocol = "ws"
  if (__ENV.ENVIRONMENT_URL.includes("https")){
    webSocketProtocol="wss"
  }
  return `${webSocketProtocol}://${__ENV.ENVIRONMENT_URL.replace(/^https?:\/\//, '')}/-/cable`;
}

export function setup() {
  console.log("");
  console.log(`RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`);
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
  console.log(`Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD) * 100}%`);

  const userGID = get_current_user_gid();
  const webSocketURL = buildWebSocketURL();
  const webSocketParams = { headers: { "Origin": __ENV.ENVIRONMENT_URL, "PRIVATE-TOKEN": access_token } };

  return {userGID: userGID, webSocketURL: webSocketURL, webSocketParams: webSocketParams}
}

export default function (data) {
  group("API - Send question and wait for answer from Duo Chat", function () {
    const gid = data.userGID;
    const clientSubscription = randomString(24); // The string can be any random string
    const questionString = "How do I create a merge request?"

    const ws = new WebSocket(data.webSocketURL, null, data.webSocketParams);
    ws.onopen = () => {
      let msg = subscribeMessage(gid,clientSubscription)
      ws.send(msg);
    };

    ws.onerror = (e) => {
      console.log("WS Error: " + JSON.stringify(e));
      successRate.add(false);
      ws.close();
    };

    ws.onmessage = (data) => {
      const messageData = data["data"]
      const dataType = JSON.parse(messageData)["type"];
      const dataMsg = JSON.parse(messageData)["message"];

      if (dataType === "ping" || dataType === "welcome"){
        // Ignore welcome/ping messages
      } else if (dataType === "confirm_subscription"){
        // With the subscription confirmed, send a question to GitLab Duo
        send_question_to_duo(gid, clientSubscription, questionString)
      } else if (dataMsg && dataMsg["result"]){

        const aiCompletionResponse = dataMsg["result"]["data"]["aiCompletionResponse"];

        if (aiCompletionResponse && aiCompletionResponse["role"]!==null && aiCompletionResponse["content"]!==null){
          // The message response with role=ASSISTANT, and a non empty payload indicates that the response has started
          // to stream, and should be rendering on the UI. As the focus for this test is to measure how long the process
          // of connecting to server, sending question, and until the response 'starts' to be rendered,
          // we can now terminate the connection.
          if (aiCompletionResponse["role"]==="ASSISTANT" && aiCompletionResponse["content"].trim()!==""){
            ws.close();
            successRate.add(true);
          }
          else if (aiCompletionResponse["role"]==="ASSISTANT" && aiCompletionResponse["content"].trim()===""){
            // Streaming response from assistant but content empty, wait a little longer for non empty payload
          }else{
            // Unexpected and unhandled response
            console.log('aiCompletionResponse["role"] --- ' + aiCompletionResponse["role"] + '  ---');
            console.log('aiCompletionResponse["content"] --- ' + aiCompletionResponse["content"] + '  ---');
            successRate.add(false);
            fail('unexpected role/content');
          }

        }else if (aiCompletionResponse === null && dataMsg["more"] === true){
          // waiting for data to stream
        } else {
          // Unexpected and unhandled response
          console.log('dataMsg["result"] --- ' + JSON.stringify(dataMsg) + ' ---');
          successRate.add(false);
          fail('Unexpected and unhandled result object in response')
        }
      }
    };
  });
}

export function get_current_user_gid() {
  const res = http.get(
      `${__ENV.ENVIRONMENT_URL}/api/v4/user/`,
      { headers: { "Accept": "application/json", "PRIVATE-TOKEN": `${__ENV.ACCESS_TOKEN}` } }
  );
  const userID = res.json()["id"]
  return `gid://gitlab/User/${userID}`
}

export function subscribeMessage(userGID, clientSubscriptionID) {

  const subscriptionQueryString = `
      subscription aiCompletionResponseStream(
      $userId: UserID, 
      $resourceId: AiModelID, 
      $agentVersionId: AiAgentVersionID, 
      $clientSubscriptionId: String, 
      $aiAction: AiAction
    ) {
      aiCompletionResponse(
        userId: $userId 
        resourceId: $resourceId 
        agentVersionId: $agentVersionId 
        aiAction: $aiAction 
        clientSubscriptionId: $clientSubscriptionId 
      ) {
        id 
        requestId 
        content 
        errors 
        role 
        timestamp 
        type 
        chunkId 
        extras {
          sources
        }
      }
    }
  `

  return JSON.stringify(
      {
        command: "subscribe",
        identifier: JSON.stringify({
          channel: "GraphqlChannel",
          query: subscriptionQueryString,
          variables: {
            userId: userGID,
            resourceId: userGID,
            clientSubscriptionId: clientSubscriptionID
          },
          operationName: "aiCompletionResponseStream"
        })
      }
  )
}

export function send_question_to_duo(
    userGID,
    clientSubscriptionID,
    content="Help me learn how to create a merge request"
){
  const graphql_mutation_chat = `
    mutation AiAction {
      aiAction(
        input: {
          chat: {
            resourceId: "${userGID}", 
            content: "${content}" 
          }
          clientSubscriptionId: "${clientSubscriptionID}"
          }
      ) {
        errors
        requestId
      }
    }
    `;

  const res = http.post(
      `${__ENV.ENVIRONMENT_URL}/api/graphql`,
      JSON.stringify({query: graphql_mutation_chat}),
      { headers: {'Authorization': `Bearer ${access_token}`, 'Content-Type': 'application/json' }}
  )

  if (res.status!==200){
    console.log("Error sending question to Duo -- " + res.status + " -- " + JSON.stringify(res));
    fail("Error sending question to Duo" + JSON.stringify(res));
    successRate.add(false);
  }

  // Graphql endpoint can return 200, but the payload contains "errors" which indicates failure
  if (res.json()["errors"]!=null){
    console.log("Error sending question to Duo -- " + res.status + " -- " + JSON.stringify(res.json()["errors"]));
    fail("Error sending question to Duo" + JSON.stringify(res.json()["errors"]));
    successRate.add(false);
  }

  return res
}

