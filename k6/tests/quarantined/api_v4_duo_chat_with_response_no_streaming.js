/*global __ENV : true  */
/*
@endpoint: `POST /api/graphql`
@description: Send message to Duo Chat via aiAction GraphQL API and check for response without streaming
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@gpt_data_version: 1
@stressed_components: Rails, Postgres
*/

import { check, group, sleep } from "k6";
import http from "k6/http";
import { Rate } from "k6/metrics";

import {
  getRpsThresholds,
  getTtfbThreshold,
  logGraphqlError,
  logError,
} from "../../lib/gpt_k6_modules.js";

export let thresholds = {
  ttfb: { latest: 1000 },
};
export let customSuccessRate = 0.95 // a small percent of requests returns undefined error due to known delay
export let rpsThresholds = getRpsThresholds(thresholds["rps"]);
export let ttfbThreshold = getTtfbThreshold(thresholds["ttfb"]);
export let successRate = new Rate("successful_requests");

// If Service Account PAT is used for GPT, AI tests require real user PAT which can be provided via AI_ACCESS_TOKEN
export const access_token =
  __ENV.AI_ACCESS_TOKEN !== null && __ENV.AI_ACCESS_TOKEN !== undefined
    ? __ENV.AI_ACCESS_TOKEN
    : __ENV.ACCESS_TOKEN;

export let options = {
  thresholds: {
    successful_requests: [`rate>${customSuccessRate}`],
    http_req_waiting: [`p(90)<${ttfbThreshold}`],
    http_reqs: [`count>=${rpsThresholds["count"]}`],
  },
};

let params = {
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${access_token}`,
  },
};

export function setup() {
  console.log("");
  console.log(
    `RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`,
  );
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
  console.log(
    `Success Rate Threshold: ${customSuccessRate*100}%`,
  );

  let userId = 1;
  let message = "Add three numbers in Python";

  let gql_query = `
        mutation chat(
            $agentVersionId: AiAgentVersionID
            $clientSubscriptionId: String
            $currentFileContext: AiCurrentFileInput
        ) {
            aiAction(
            input: {
                chat: {
                resourceId: "gid://gitlab/User/${userId}"
                content: "${message}"
                agentVersionId: $agentVersionId
                currentFile: $currentFileContext
                }
                clientSubscriptionId: $clientSubscriptionId
            }
            ) {
            requestId
            errors
            }
        }`;

  let res = http.post(
    `${__ENV.ENVIRONMENT_URL}/api/graphql`,
    JSON.stringify({ query: gql_query }),
    params,
  );

  let requestId =
    typeof JSON.parse(res.body).data.aiAction.requestId !== "undefined"
      ? JSON.parse(res.body).data.aiAction.requestId
      : null;
  return { requestId };
}

export default function (data) {
  group("API - Duo Chat aiMessages Query", function () {
    let gql_query = `
    query getAiMessages {
        aiMessages(requestIds: "${data.requestId}", roles:ASSISTANT) {
            nodes {
                id
                requestId
                content
                contentHtml
                errors
                role
                timestamp
                extras {
                  sources
                  hasFeedback
                }
            }
        }
        }
    `;

    let res = http.post(
      `${__ENV.ENVIRONMENT_URL}/api/graphql`,
      JSON.stringify({ query: gql_query }),
      params,
    );

    checkForContent(res, 30); // wait no more than 3 seconds

    const checkOutput = check(res, {
      "is status 200": (r) => r.status === 200,
      "verify response is received": (r) =>
        typeof JSON.parse(r.body).data.aiMessages.nodes[0] !== "undefined" &&
        typeof JSON.parse(r.body).data.aiMessages.nodes[0].content !== "undefined" &&
        JSON.parse(r.body).data.aiMessages.nodes[0].content !== "",
    });

    checkOutput
      ? successRate.add(true)
      : (successRate.add(false), logError(res));

    const message = JSON.parse(res.body).data.aiMessages.nodes[0];
    const graphQLErrors = (typeof message === "undefined") ? "NO MESSAGE" : message.errors;
    (graphQLErrors === "NO MESSAGE") || (typeof graphQLErrors[0] !== "undefined")
        ? (successRate.add(false), logGraphqlError(graphQLErrors))
        : successRate.add(true);
  });
}

// It takes a few moments for the Duo Chat response to become available
// so we need to check for the content at intervals.
// If a response isn't available after 3 seconds we consider it a failure.
function checkForContent(res, i) {
  if(typeof JSON.parse(res.body).data.aiMessages.nodes[0] === "undefined") {
    while (i-- > 0) {
      sleep(0.1);
      checkForContent(res);
    }
  }
}
